mod domain;

pub struct RobotState;

pub struct RTDEReceiveInterface {
}

#[allow(non_snake_case)]
impl RTDEReceiveInterface {
    
    // pub fn disconnect(&mut self) { todo!() }
    // pub fn reconnect(&mut self) -> std::io::Result<()> { todo!() }
    // pub fn startFileRecording(&mut self, ) -> Vec<f64> { todo!() }

    pub fn isConnected(&self) -> bool { todo!() }
    pub fn getTimestamp(&self) -> f64 { todo!() }
    pub fn getTargetQ(&self) -> Vec<f64> { todo!() }
    pub fn getTargetQd(&self) -> Vec<f64> { todo!() }
    pub fn getTargetQdd(&self) -> Vec<f64> { todo!() }
    pub fn getTargetCurrent(&self) -> Vec<f64> { todo!() }
    pub fn getTargetMoment(&self) -> Vec<f64> { todo!() }
    pub fn getActualQ(&self) -> Vec<f64> { todo!() }
    pub fn getActualQd(&self) -> Vec<f64> { todo!() }
    pub fn getActualCurrent(&self) -> Vec<f64> { todo!() }
    pub fn getJointControlOutput(&self) -> Vec<f64> { todo!() }
    pub fn getActualTCPPose(&self) -> Vec<f64> { todo!() }
    pub fn getActualTCPSpeed(&self) -> Vec<f64> { todo!() }
    pub fn getActualTCPForce(&self) -> Vec<f64> { todo!() }
    pub fn getTargetTCPPose(&self) -> Vec<f64> { todo!() }
    pub fn getActualDigitalInputBits(&self) -> u64 { todo!() }
    pub fn getJointTemperatures(&self) -> Vec<f64> { todo!() }
    pub fn getActualExecutionTime(&self) -> f64 { todo!() }
    pub fn getRobotMode(&self) -> i32 { todo!() } // Todo use enum later for RobotMode
    pub fn getRobotStatus(&self) -> u32 { todo!() } // Todo use enum later for RobotStatus
    pub fn getJointMode(&self) -> Vec<i32> { todo!() }
    pub fn getSafetyMode(&self) -> i32 { todo!() }
    pub fn getSafetyStatusBits(&self) -> u32 { todo!() }
    pub fn getActualToolAccelerometer(&self) -> Vec<f64> { todo!() }
    pub fn getSpeedScaling(&self) -> f64 { todo!() }
    pub fn getTargetSpeedFraction(&self) -> f64 { todo!() }
    pub fn getActualMomentum(&self) -> f64 { todo!() }
    pub fn getActualMainVoltage(&self) -> f64 { todo!() }
    pub fn getActualRobotVoltage(&self) -> f64 { todo!() }
    pub fn getActualRobotCurrent(&self) -> f64 { todo!() }
    pub fn getActualJointVoltage(&self) -> Vec<i32> { todo!() }
    pub fn getActualDigitalOutputBits(&self) -> u64 { todo!() }
    pub fn getDigitalOutState(&self, output_id: u8) -> bool { todo!() }
    pub fn getRuntimeState(&self) -> u32 { todo!() }
    pub fn getStandardAnalogInput0(&self) -> f64 { todo!() }
    pub fn getStandardAnalogInput1(&self) -> f64 { todo!() }
    pub fn getStandardAnalogOutput0(&self) -> f64 { todo!() }
    pub fn getStandardAnalogOutput1(&self) -> f64 { todo!() }
    pub fn isProtectiveStopped(&self) -> bool { todo!() }
    pub fn isEmergencyStopped(&self) -> bool { todo!() }
    pub fn getOutputIntRegister(&self, output_id: i32) -> i32 { todo!() }
    pub fn getOutputDoubleRegister(&self, output_id: i32) -> f64 { todo!() }
    pub fn getAsyncOperationProgress(&self) -> i32 { todo!() }
    pub fn getSpeedScalingCombined(&self) -> f64 { todo!() }
    pub fn getFtRawWrench(&self) -> Vec<f64> { todo!() }
    pub fn receiveCallback(&self) { todo!() }
    pub fn recordCallback(&self) { todo!() }
    pub fn robot_state(&self) -> &RobotState { todo!() }
}