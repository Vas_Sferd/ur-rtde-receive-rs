use super::enums;

pub struct RobotState {
    timestamp: f64,
    target_q: Vec<f64>,
    target_qd: Vec<f64>,
    target_qdd: Vec<f64>,
    target_current: Vec<f64>,
    target_moment: Vec<f64>,

    actual_q: Vec<f64>,
    actual_qd: Vec<f64>,
    actual_qdd: Vec<f64>,
    actual_current: Vec<f64>,
    actual_moment: Vec<f64>,

    joint_control_output: Vec<f64>,

    actual_tcp_pose: Vec<f64>,
    actual_tcp_speed: Vec<f64>,
    actual_tcp_force: Vec<f64>,
    target_tcp_pose: Vec<f64>,
    target_tcp_speed: Vec<f64>,

    actual_digital_input_bits: u64,

    joint_temperatures: Vec<f64>,
    actual_execution_time: f64,
    robot_mode: enums::RobotMode,
    joint_mode: Vec<i32>,
    safety_mode: i32,
    actual_tool_accelerometer: Vec<f64>,
    speed_scaling: f64,
    target_speed_fraction: f64,
    actual_momentum: f64,
    actual_main_voltage: f64,
    actual_robot_voltage: f64,
    actual_robot_current: f64,
    actual_joint_voltage: Vec<f64>,
    actual_digital_output_bits: u64,
    runtime_state: u32,
    robot_status_bits: u32,
    
    standard_analog_input0: f64,
    standard_analog_input1: f64,
    standard_analog_output0: f64,
    standard_analog_output1: f64,

    ft_raw_wrench: Vec<f64>,

    output_bit_registers0_to_31: u32,
    output_bit_registers32_to_63: u32,

    output_int_register: [i32; 48],
    output_double_register: [f64; 48],
}