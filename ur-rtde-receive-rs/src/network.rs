use std::{
    io::prelude::*,
    net::{TcpStream, SocketAddr},
    time::Duration,
};


pub struct RtdeReciveApi(TcpStream);

impl RtdeReciveApi {
    fn connect_to_addr(addr: &SocketAddr) -> std::io::Result<Self> {
        let mut stream = TcpStream::connect_timeout(addr, Duration::from_secs(2))
            .expect("Couldn't connect to the server...");

        Ok(
            Self(
                stream
            )
        )
    }

}
